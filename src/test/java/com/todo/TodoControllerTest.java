package com.todo;

import com.todo.models.TodoItem;
import com.todo.models.TodoList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Objects;

import static com.todo.TodoFixtures.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer.sharedHttpSession;

@SpringBootTest
@AutoConfigureMockMvc
class TodoControllerTest {

    MockMvc mockMvc;

    @Autowired
    WebApplicationContext applicationContext;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(sharedHttpSession()).build();
    }

    @Test
    public void canOpenAddTodoPage() throws Exception {
        var result = mockMvc.perform(get("/todos/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("todo"))
                .andReturn();

        var item = (TodoItem) Objects.requireNonNull(result.getModelAndView()).getModel().get("todo");

        assertTrue(StringUtils.isBlank(item.getTitle()));
        assertTrue(StringUtils.isBlank(item.getDescription()));
    }

    @Test
    public void canOpenTodoListPage() throws Exception {
        var result = mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andReturn();
        var todos = (TodoList) Objects.requireNonNull(result.getModelAndView()).getModel().get("todos");

        assertEquals(0, todos.size());
    }

    @Test
    public void whenAddTodo_updateTheListAndRedirectToTodosPage() throws Exception {
        mockMvc.perform(post("/todos/add")
                        .param("title", aTodo.getTitle())
                        .param("description", aTodo.getDescription()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(""))
                .andReturn();

        var result = mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andReturn();
        var todos = (TodoList) Objects.requireNonNull(result.getModelAndView()).getModel().get("todos");

        assertEquals(todos.size(), 1);
        var firstTodo = todos.getFirst();
        assertEquals(firstTodo.getTitle(), aTodo.getTitle());
        assertEquals(firstTodo.getDescription(), aTodo.getDescription());
    }

    @Test
    public void whenDeleteTodo_subsequentCallsMustHaveNoTodo() throws Exception {
        var sessionAttributes = new MockHttpSession();
        sessionAttributes.setAttribute("todos", aTodoList);

        mockMvc.perform(post("/todos/delete")
                        .session(sessionAttributes)
                        .param("id", aTodo.getId().toString()))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl(""))
                .andReturn();

        var result = mockMvc.perform(get("/todos"))
                .andExpect(status().isOk())
                .andReturn();
        var todos = (TodoList) Objects.requireNonNull(result.getModelAndView()).getModel().get("todos");
        assertEquals(todos.size(), 1);
        assertEquals(todos.getFirst(), anotherTodo);
    }
}