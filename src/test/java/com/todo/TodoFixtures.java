package com.todo;

import com.todo.models.TodoItem;
import com.todo.models.TodoList;

public class TodoFixtures {
    public static TodoItem aTodo = new TodoItem(1, "Title", "Description");
    public static TodoItem anotherTodo = new TodoItem(2, "Another Title", "Another Description");
    public static TodoList aTodoList = new TodoList();

    static {
        aTodoList.add(aTodo);
        aTodoList.add(anotherTodo);
    }
}
