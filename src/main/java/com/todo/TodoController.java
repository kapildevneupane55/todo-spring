package com.todo;

import com.todo.models.TodoItem;
import com.todo.models.TodoList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@Controller
@RequestMapping("/todos")
@SessionAttributes("todos")
public class TodoController {
    @ModelAttribute("todos")
    public TodoList todos() {
        return new TodoList();
    }

    @GetMapping
    public String showTodoList(@ModelAttribute("todos") TodoList todos) {
        return "todos";
    }

    @GetMapping("/add")
    public String addTodo(Model model, @ModelAttribute("todos") TodoList todos) {
        model.addAttribute("todo", new TodoItem());
        return "add-todo";
    }

    @PostMapping(value = "/add")
    public String addTodo(
            TodoItem todo,
            @ModelAttribute("todos") TodoList todos
    ) {
        todo.setCreatedTime(LocalDateTime.now());
        todos.add(todo);

        return "redirect:";
    }

    @PostMapping("/delete")
    public String deleteTodo(TodoItem todo, @ModelAttribute("todos") TodoList todos) {
        todos.remove(todo);

        return "redirect:";
    }

    @PostMapping("/done")
    public String markAsDone(TodoItem todo, @ModelAttribute("todos") TodoList todos) {
        todos.markAsDone(todo);

        return "redirect:";
    }
}
