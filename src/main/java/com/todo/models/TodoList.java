package com.todo.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class TodoList implements Iterable<TodoItem> {
    public static Integer id = 1;
    private final List<TodoItem> todoList = new ArrayList<>();

    public boolean add(TodoItem todo) {
        if (todo.getId() == null) {
            todo.setId(id);
            id = id + 1;
        }
        return this.todoList.add(todo);
    }

    public void remove(TodoItem todo) {
        var success = this.todoList.remove(todo);
        if (!success) throw new RuntimeException("Todo not found");
    }

    public void markAsDone(TodoItem todo) {
        var todoFromList = this.todoList.stream()
                .filter(todoItem -> Objects.equals(todoItem.getId(), todo.getId()))
                .findFirst().orElseThrow();
        todoFromList.setCompleted(todo.getCompleted());
    }

    public int size() {
        return this.todoList.size();
    }

    public TodoItem getFirst() {
        return this.todoList.get(0);
    }

    @Override
    public Iterator<TodoItem> iterator() {
        return this.todoList.iterator();
    }
}
