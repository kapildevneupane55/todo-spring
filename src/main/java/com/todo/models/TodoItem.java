package com.todo.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class TodoItem {
    private Integer id;
    private String title;
    private String description;
    private LocalDateTime createdTime;
    private Boolean completed = false;

    public TodoItem() { }

    public TodoItem(Integer id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.createdTime = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object another) {
        if (!(another instanceof TodoItem)) {
            return false;
        }
        var castedAnother = (TodoItem) another;
        return Objects.equals(this.id, castedAnother.id);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
}
